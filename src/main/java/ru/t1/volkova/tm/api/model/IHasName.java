package ru.t1.volkova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
