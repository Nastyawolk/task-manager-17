package ru.t1.volkova.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void unbindTaskFromProject(String projectId, String taskId);

}
