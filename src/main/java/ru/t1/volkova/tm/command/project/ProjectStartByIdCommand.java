package ru.t1.volkova.tm.command.project;

import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Start project by id.";

    private static final String NAME = "project-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        projectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
