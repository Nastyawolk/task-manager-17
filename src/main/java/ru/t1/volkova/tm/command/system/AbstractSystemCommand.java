package ru.t1.volkova.tm.command.system;

import ru.t1.volkova.tm.api.service.ICommandService;
import ru.t1.volkova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
