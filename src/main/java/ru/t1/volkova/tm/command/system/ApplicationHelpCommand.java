package ru.t1.volkova.tm.command.system;

import ru.t1.volkova.tm.api.model.ICommand;
import ru.t1.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-h";

    private static final String DESCRIPTION = "Show commands list.";

    private static final String NAME = "help";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
