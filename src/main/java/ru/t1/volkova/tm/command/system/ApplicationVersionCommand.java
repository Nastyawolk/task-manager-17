package ru.t1.volkova.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-v";

    private static final String DESCRIPTION = "Show program version.";

    private static final String NAME = "version";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.17.0");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
