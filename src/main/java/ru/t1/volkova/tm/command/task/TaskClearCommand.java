package ru.t1.volkova.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Remove all tasks.";

    private static final String NAME = "task-clear";

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        taskService().clear();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
