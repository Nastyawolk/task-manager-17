package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Complete task status by index.";

    private static final String NAME = "task-complete-by-index";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
