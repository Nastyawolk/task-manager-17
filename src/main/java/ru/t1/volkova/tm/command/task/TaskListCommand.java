package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.enumerated.Sort;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Show tasks list.";

    private static final String NAME = "task-list";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService().findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
