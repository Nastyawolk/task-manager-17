package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Remove task by id.";

    private static final String NAME = "task-remove-by-id";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService().removeById(id);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
