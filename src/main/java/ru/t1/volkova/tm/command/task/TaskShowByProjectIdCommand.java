package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Show tasks by project id.";

    private static final String NAME = "task-show-by-project-id";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
